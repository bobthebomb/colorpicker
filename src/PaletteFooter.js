import React from "react"
import { styled, Box } from "@mui/system";
import { Link, useParams} from "react-router-dom"



function PaletteFooter(props) {


    // En vrai je passe la props de la color et de la palette donc y'a pas besoin de userParams(). Mais si je veux re utiliser le component footer dans une autre feature, le userParams sera tjs fonctionnel.
    let histoire = useParams();
    let backLink = histoire.colorId ?  `/${histoire.paletteId}` : "/"

    const Footer = styled("footer")({
        height: "6%",
        backgroundColor: "white",
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        fontWeight: "bold",
        border: "dashed black 1px"
    })

    const Emoji = styled("span")({
        height: "100%",

        fontSize: "1.5rem",
        margin: "0 0.5rem",
        alignItems: "center",
        display: "flex",
        marginBottom: "0.5rem"

    })


    const leftBoxCss = {
        height: "100%",       
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        width: "100%",


    }

    const leftLink = {
        textDecoration: "none", 
        color: "black",
        display: "flex",
        alignItems: "center",
        paddingLeft: "1rem"
    }

    const  {paletteName,emoji } = props;

    return(
        <Footer >
            <Box sx={leftBoxCss} >
                 <Link style={leftLink} to={backLink}> 
                    Back
                </Link>

                {paletteName}
            </Box>
                
            <Emoji > {emoji}</Emoji>
            
            
        </Footer>

    )
    
} export default PaletteFooter;