import React, { Component } from 'react';
import NewPaletteDrawer from "./NewPaletteDrawer";
import SeedColors from "./SeedColors";


class NewPaletteForm extends Component{
    
    constructor(props){
        super(props)
        this.state = {
            colors: [
                {color : "red" ,
                name: "red",
                id: "1"},

                {color : "blue" ,
                name: "blue",
                id: "2"},

                {color : "green" ,
                name: "green",
                id: "3"},

                {color : "purple" ,
                name: "purple",
                id: "4"},

                {color : "yellow" ,
                name: "yellow",
                id: "5"},
                
                {color : "teal" ,
                name: "teal",
                id: "6"},
                
                {color : "orange" ,
                name: "orange",
                id: "7"},
                
                {color : "lightGrey" ,
                name: "lightGrey",
                id: "8"},
                
                {color : "brown" ,
                name: "brown",
                id: "9"},
                
                {color : "pink" ,
                name: "pink",
                id: "10"},
                
            ],
            newColorName: "",
            newPaletteName: ""
        }
    }


    addNewColor = (currentColor, colorNewName) => {
  
        this.setState({
            colors : [...this.state.colors, 
                {
                    color : currentColor,
                    name: colorNewName,
                    id: `${currentColor}${colorNewName}`
                   
                } ] ,
            newColorName : ""
        })
    }

    addNewName = (name, value) => {
        this.setState({
            [name]: value
        })
    }

    deleteNewColor = (colorName) => {
        this.setState({
            colors: this.state.colors.filter(color => color.name !== colorName )
        })
    }

    clearColors = () => {
        this.setState({
            colors : []
        })
    }

    randomColors = () => {
        const allColors = SeedColors.map( p => p.colors).flat()
        const allColorsWithId = allColors.map( oneColor => oneColor = {...oneColor, "id" : oneColor.name} )
        
        let isDuplicate = true

        while (isDuplicate === true) {

            const rand = Math.floor(Math.random()*allColorsWithId.length - 1 )      
            const randomColor = allColorsWithId[rand]
            const isNotInState = (currentValue) => currentValue.name !== randomColor.name

            if(this.state.colors.every(isNotInState) ) {
                this.setState({
                    colors : [...this.state.colors, randomColor]
                })
                isDuplicate = false
            } else {
                console.log("reroll" + randomColor.name)
            }
            
        }

        
    }
 


    render(){

        const {savePalette, palettes} = this.props

        return(
            <div>
                <NewPaletteDrawer 
                addNewColor={this.addNewColor} 
                deleteNewColor={this.deleteNewColor}
                colors={this.state.colors}
                addNewName={this.addNewName}
                newColorName={this.state.newColorName}
                newPaletteName={this.state.newPaletteName}
                savePalette={savePalette}
                palettes={palettes}
                clearColors={this.clearColors}
                randomColors={this.randomColors}
                />
            </div>
        )
    }



} export default NewPaletteForm;