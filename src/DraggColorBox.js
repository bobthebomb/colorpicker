import React from 'react';
import {  styled } from '@mui/material/styles';
import DeleteIcon from '@mui/icons-material/Delete';
import { useSortable } from '@dnd-kit/sortable';
import { CSS } from '@dnd-kit/utilities'
import Box from '@mui/material/Box';
import chroma from "chroma-js"

function DraggColorBox(props) {

    const {id, color, colorName, deleteNewColor, itemRemove }  = props


    const isDarkColor = chroma(color).luminance() <= 0.1

    const {
        setNodeRef,
        attributes,
        listeners,
        transition,
        transform,
        } = useSortable({id: id ,
            transition: {
                duration: 250, // milliseconds
                easing: 'cubic-bezier(0.25, 1, 0.5, 1)',
            },
            })


            // theme is needed until miu bugfix (seach github for "miu theme viewsize bug")
    const styleBox = ( theme ) => ({
     
        transform: CSS.Transform.toString(transform),   
        transition,   
        
        display: "inline-block",
        position: "relative",
        cursor: "pointer",
        marginBottom: "-7px",      
        backgroundColor: color,
        width: { xs: "100%", sm: "100%", md:"50%", lg: "25%", xl: "20%"   },
        height: { xs: "10%", sm: "5%", md:"10%", lg: "20%", xl: "25%"   },
        minHeight : "50px",
    })
   

    const BoxContent = styled('div')({
        position: "absolute",
        padding: "10px",
        width: "100%",
        left: "0px",
        bottom: "0px",     
        letterSpacing: "1px",
        textTransform: "uppercase",
        fontSize: "12px",
        display: "flex",
        justifyContent: "space-between",
        "&:hover svg": {
            color: "white"
        }
    })

    const RenderNameWithContrast = styled("span")({
        color: isDarkColor ? "white" : "black"
    })
    const iconStyle = {
        transition: "all 0.3s ease-in-out",
        color: isDarkColor ? "rgba(255,255,255,0.5)" : "rgba(0,0,0,0.5)"

    }

 const renderName = colorName ? colorName : color

    const handleDeleteNewColorBox = (e) => {
        deleteNewColor(e)
        itemRemove(e)
    }

  

    return(     
                   <Box 
                        className="drag-box-div"
                        sx={styleBox}
                        ref={setNodeRef}
                        {...attributes}
                        {...listeners}
                        >
                            <BoxContent>   
                                <RenderNameWithContrast >{renderName}</RenderNameWithContrast>
                                <DeleteIcon id="test" sx={iconStyle} onClick={()=>handleDeleteNewColorBox(colorName)}/>
                            </BoxContent>                          
                    </Box>

         
       
    )   
}
export default DraggColorBox ; 