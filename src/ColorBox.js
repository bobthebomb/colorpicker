import React, { Component } from 'react';
import './ColorBox.css'
import {CopyToClipboard} from "react-copy-to-clipboard"
import { Link } from "react-router-dom";
import chroma from "chroma-js"

class ColorBox extends Component{
    constructor(props){
        super(props)
        this.state = {
            copied : false
        }
    }

    changeCopyState = () => {
        this.setState({
            copied : true
        }, ()=> setTimeout(() => this.setState({copied : false}),700));  
    }

    /* prevent the link to also copy because he's inside CopyToClipboard*/
    handleStop = (e) => {
        e.stopPropagation();
    }


    render(){
        /* dans le copyoverlay je need les deux classname de css pour que ma transition fonctionne !!! voir les classes de css pour les détails si je remplace copy-overlay par copy-overlay-show la transition ne s'oppere pas */
        const {name, background, paletteId, colorId, moreLink} = this.props;
        const {copied } = this.state

        const copyOverlay = copied ? "copy-overlay-show copy-overlay" : "copy-overlay"
        const copyOverlayText = copied ? "copy-message-show copy-message" : "copy-message"
        
        const isDarkColor = chroma(background).luminance() <= 0.1
        const isLightColor = chroma(background).luminance() >= 0.5

        return(
            <CopyToClipboard text={background} onCopy={this.changeCopyState}>
                <div style={{background, marginBottom: "-4px"}} className="ColorBox">
                    <div style={{background}} className={copyOverlay}>
                    </div>
                    <div className={copyOverlayText}>
                        <h1 className={`${isLightColor && "dark-text"}`}> Copied </h1>
                        <p className={`${isLightColor && "dark-text"}`}> {background} </p>
                    </div>

                        <div className="copy-container"> 
                            <div className="box-content">
                                <span className={`${isDarkColor && "light-text"}`}>{name} </span>
                            </div>
                            <button className={`copy-button ${isLightColor && "dark-text"}`}>Copy</button>
                        </div>
                       {moreLink && <Link to={`/palette/${paletteId}/${colorId}`} onClick={this.handleStop}><span className={`see-more ${isLightColor && "dark-text"}  `}>MORE</span> </Link>}
                </div>
            </CopyToClipboard>
            
        )
    }
} export default ColorBox;

