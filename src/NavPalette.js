import React from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Palette from "./Palette"
import {generatePalette} from "./colorHelpers"


function NavPalette(props) {



    function findPalette(id) {
        return  props.seedColors.find( element => element.id === id)    
      }
        

    const params = useParams()

    return (
     
          <Palette palette={generatePalette(findPalette(params.paletteId))}/>

    
    )
    
} export default NavPalette;
