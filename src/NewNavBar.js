import React from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import { Box, Button } from '@mui/material';
import { Link } from "react-router-dom";
import PaletteMetaForm from "./PaletteMetaForm"


function NewNavBar(props) {

    const { open, AppBar, handleChange, handleDrawerOpen, savePalette , newPaletteName, colors, palettes, addNewName} = props


    const MainDiv = {
        display: "flex",
    }

    const NavButtons = {
        display: "flex",
        flexDirection: "row",
        justifyContent: "end",
        width: "100%",
        marginRight : {xs:"0rem", sm: "0rem", md: "0.5rem" , lg: "1rem"     }
    }

   const homeButton = { 
    marginLeft: {xs:"0.5rem", sm: "0.5rem", md: "1rem" , lg: "1.5rem"     }
   }

   // Encore une occurence de loose focus avec Styled de material-ui. En l'espece mon dialog(popup) perd le focus et donc donc close a chaque click (et re -mount en plus)

   const hiddenText = { xs: "none", sm: "none",  md: "initial"}


    return(

        <Box sx={MainDiv}>
            <CssBaseline />
            <AppBar position="fixed" open={open} color="default">
                <Toolbar display="flex">
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        sx={{ mr: 2, ...(open && { display: 'none' }) }}
                    >
                    <MenuIcon />

                    </IconButton>

                    <Typography sx={{display: hiddenText }}variant="h6" noWrap component="div" width="12%">
                        Create Palette
                    </Typography>

                  

                     <Box sx={NavButtons}>     

                       <PaletteMetaForm 
                            open={open} 
                            AppBar={AppBar} 
                            handleDrawerOpen={handleDrawerOpen} 
                            savePalette={savePalette} 
                            handleChange={handleChange}
                            palettes={palettes}
                            newPaletteName={newPaletteName}
                            colors={colors}
                            addNewName={addNewName}
          
                        />   

                        <Button  variant="contained" color="secondary" sx={homeButton} >
                                <Link to="/" style={{textDecoration:"none", color: "white" }} >
                                    Home 
                                </Link>
                        </Button>

                    </Box>              
                               
                </Toolbar>   

                  
            </AppBar>
            
        </Box>


    )
    
} export default NewNavBar;