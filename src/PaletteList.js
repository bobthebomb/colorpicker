import React, { Component } from 'react';
import { Link} from "react-router-dom";
import MiniPalette from './MiniPalette';
import { styled } from "@mui/system";
import bg2 from "./bg2.svg"
import { TransitionGroup,} from 'react-transition-group';
import Fade from '@mui/material/Fade';
import "./PaletteList"; 
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import { Avatar } from '@mui/material';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemText from '@mui/material/ListItemText';
import CheckIcon from '@mui/icons-material/Check';
import CancelIcon from '@mui/icons-material/Cancel';
import { blue, red } from '@mui/material/colors'

   /* background by SVGBackgrounds.com !!! my review : best site ever for background generation !!*/


const Root = styled("div")({
    backgroundColor: "#110633",
    backgroundImage: `url(${bg2})`,
    height: "100%",
    display: "flex",
    alignItems: "flex-start",
    justifyContent: "center",
    overflow: "scroll"
})

const Container = styled("div")( ({theme}) =>( {
    
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    flexWrap: "wrap",
    [theme.breakpoints.up("lg")]:{ width: "50%"},
    [theme.breakpoints.down("lg")]:{ width: "80%"},

}))

const Nav = styled("nav")({
    display: "flex",
    width: "100%",
    justifyContent: "space-between",
    color: "white",
    alignItems: "baseline",
    

})

const TitleMain = styled("h1")( ({theme})=>(  {
    fontSize: "2rem",
    [theme.breakpoints.down(500)]: {fontSize: "1.5rem"},
    [theme.breakpoints.down(360)]: {fontSize: "1rem"}

}))

const newPaletteLinkSx = {
    fontSize: "1rem",
    textDecoration: "underline",
    color: "#FFFFFF"
}

// Comme c'est une component de MIU 5 impossible de l'utiliser avec le transition group. Si je le passe en props style le helper breakpoints revoin min-width instead of minWidth.
// Faudrait utiliser un hook media querry et une lib (json2mq) pour faire les request en json mais ca marche que dans une functionnal component(c'est un hook).
 // Par chance TransitionGroup propose un component null pour eviter de bugger le css mais sinon il 
// faudrait faire une paletteList.css et faire les media querry la ou faire un HOC du component, bref encore une galère de MIU5.
// d'un autre coté MIU5 a un Fade api qui fonctionne avec Transition group et c'est niquel ! 

const Palette = styled("div")(({theme}) =>( {
    boxSizing: "border-box",
    width: "100%",
    gridGap: "1.5rem",
    display: "grid",
    gridTemplateColumns: "repeat(3, 31.5%)",
    [theme.breakpoints.down(800)]:{ gridTemplateColumns: "repeat(2, 50%)", gridGap: "1rem"},
    [theme.breakpoints.down(480)]:{ gridTemplateColumns: "repeat(1, 100%)", gridGap: "0.8rem"},
  
}))




class PaletteList extends Component{
    constructor(props){
        super(props)
        this.state = {
            open: false,
            deleteTarget : ""
        }
    }

  
    handleClose = () => {
        this.setState({
            open: false,
            deleteTarget: ""
        })
    }
    
    handleOpen = (id) => {
        this.setState({
            open: true,
            deleteTarget: id
        })
    }

    handleSaveConfirmation = () => {
        this.props.deletePalettes(this.state.deleteTarget)
        this.handleClose()
        
    }

    render(){

        

        const  {seedColors}  = this.props

        const paletteListing = seedColors.map( unePalette => {
            return (
                     <Fade key={unePalette.id} in={true} timeout={600}>
                        <Link 
                            key={unePalette.id}  
                            to={`/palette/${unePalette.id}`} 
                            style={{textDecoration: "none"}}>
                            <MiniPalette  {...unePalette} deletePalettes={this.handleOpen}/> 
                        </Link>               
                    </Fade>
              
                    
            )
        } )

        


        return(
            <Root>
                <Container>
                    <Nav className="nav"> 
                        <TitleMain> Palette  Colors  </TitleMain>
                            <Link style={newPaletteLinkSx} to="/palette/new"> Create Palette </Link> 
                    </Nav>
                            <Palette>
                                <TransitionGroup component={null}>
                                {paletteListing}
                                </TransitionGroup> 
                            </Palette>
                            
                </Container> 

                <Dialog
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="Alert-Delete-Confirmation"
                >
                    <DialogTitle id="delete-dialog-title" sx={{color: "red"}}> Delete this Palette ? </DialogTitle>
                    <List>

                        <ListItem button onClick={this.handleSaveConfirmation}> 
                            <ListItemAvatar>
                                <Avatar sx={{backgroundColor: blue[100], color: blue[600]}}>
                                    <CheckIcon />
                                </Avatar>                              
                            </ListItemAvatar>
                            <ListItemText sx={{color: "black"}}>
                                DELETE
                            </ListItemText>
                        </ListItem>

                        <ListItem button onClick={this.handleClose} autoFocus> 
                            <ListItemAvatar>
                                <Avatar sx={{backgroundColor: red[100], color: red[600]}} >
                                    <CancelIcon />
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText sx={{color: "black"}}>
                                 CANCEL 
                            </ListItemText>
                        </ListItem>


                    

                        
                    </List>
                   
                    
                </Dialog>                                                      
            </Root>
        )
    }
} export default PaletteList;


