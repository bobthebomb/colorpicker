import * as React from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { useEffect } from "react"
import { useNavigate } from 'react-router-dom';
import 'emoji-mart/css/emoji-mart.css'
import { Picker } from 'emoji-mart'

 function PaletteMetaForm(props) {

 const { savePalette , newPaletteName, colors, palettes, addNewName} = props
 const navigate = useNavigate()


  const [open, setOpen] = React.useState(false);
  const [emojiOpen, setEmojiOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleEmojiClose = () => {
      setEmojiOpen(false)
      setOpen(false)
  }

  const handleClose = () => {
    setOpen(false);
  };

  const handleSaveFirstStep = () => {
    setEmojiOpen(true)
  }

  const handleChange = (evt) => {
    addNewName(evt.target.name, evt.target.value)
  }


  useEffect(
    () => {
        ValidatorForm.addValidationRule('paletteNameUnique', (value) => {
            return palettes.every(
              ({paletteName}) => paletteName.toLowerCase() !== value.toLowerCase()
            )
          })
    } , []
)
  

const handleSavePalette = (emoji) => {
    const newPalette = {
      paletteName : newPaletteName,
      id: newPaletteName.toLowerCase().replace(/ /g, "-"),
      colors : colors,
      emoji: emoji.native
    }
    savePalette(newPalette)
    navigate("/")
  }

  const saveEmoji = (emoji) => {
    handleSavePalette(emoji)
  }

  const buttonsAction = {
      display: "flex",
      justifyContent: "center",
  }

  const dialogTitle = {
    display: "flex",
    justifyContent: "center",
  }

  return (

    <div>
        <Button variant="contained" color="primary" onClick={handleClickOpen}>
            Save  
        </Button>

        <Dialog open={open} onClose={handleClose}>

            <DialogTitle sx={dialogTitle}>Enter Palette Name  </DialogTitle>

            <ValidatorForm onSubmit={handleSaveFirstStep} >

                <DialogContent>
                    <DialogContentText>
                        please enter a name for your new palette !  
                    </DialogContentText>

                    <TextValidator 
                        fullWidth
                        variant="filled"
                        margin="normal"
                        name="newPaletteName" 
                        label="Palette Name" 
                        value={newPaletteName} 
                        onChange={handleChange}
                        validators={["required" , "paletteNameUnique" ]}
                        errorMessages={["can't be blank", "Palette Name already used"]}
                    />
                                    
                
                

                
                </DialogContent>

                <DialogActions sx={buttonsAction}>
                    <Button variant="contained" color="primary"  type="submit">
                        Save Palette
                    </Button>  

                    <Button variant="contained" color="error" onClick={handleClose}>
                        Cancel Saving
                    </Button>

                </DialogActions>

            </ValidatorForm>
        
        </Dialog>

        <Dialog open={emojiOpen} >
            <DialogTitle style={{display: "flex", justifyContent: "center" }}>
                Pick one Emoji
            </DialogTitle>
            <Picker
                theme="auto"
                title="Pick One Emoji"
                onSelect={saveEmoji} 
             />

            <Button variant="contained" color="error" onClick={handleEmojiClose}>
                Cancel            
            </Button>

        </Dialog>   
    </div>
  );
}

export default PaletteMetaForm;