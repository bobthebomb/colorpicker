import React, { Component } from 'react';
import ColorBox from "./ColorBox";
import NavBar from './NavBar';
import PaletteFooter from './PaletteFooter';
import { styled, Box } from "@mui/system";
import { TransitionGroup,} from 'react-transition-group';
import Fade from '@mui/material/Fade';




class Palette extends Component{
    constructor(props){
        super(props)
        this.state = {
            level : 500,
            format : "hex"
        }
    }

    changeLevel = (newLevel) => {
        this.setState({
            level : newLevel
        })
    }


    changeFormat = (event) => {
       this.setState({
           format : event
       })
    }

    
    

    render(){


        const UnePalette = styled("div")({
            height:" 100%",
            display: "flex",
            flexDirection: "column",
                })

        const { palette } = this.props
        const {colors, paletteName, emoji, id} = palette
        const {level , format }= this.state
        
        /*  colors c'est un array d objet avec des couleurs bla bla bla qui sont généré par les fonctions du colorHelpers  */
        const colorboxes = colors[level].map( color => {
          return(
                    <ColorBox 
                    paletteId={id} 
                    colorId={color.id}  
                    background={color[format]} 
                    name={color.name} 
                    key={color.id}
                    moreLink={true}
                    />
          
           ) 
        })

        return(
            <TransitionGroup component={null}>
                <Fade key={"palettename"} timeout={500}>
                    <UnePalette>  
                                
                        <NavBar 
                            level={level} 
                            changeLevelParent={this.changeLevel} 
                            handleChange={this.changeFormat}
                            lvlBarHandle={true}

                        />

                        <Box sx={{height: "88%"}} >
                                    {colorboxes}
                        </Box>

                    
                        <PaletteFooter 
                        paletteName={paletteName} 
                        emoji={emoji}
                        />  
                    
                        
                            
                    </UnePalette>
                </Fade>                
            </TransitionGroup>
           
      
     
        )
    }
} export default Palette ;