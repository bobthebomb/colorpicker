import React, {  useEffect, useState } from 'react';
import DraggColorBox from "./DraggColorBox"
import {closestCenter, DndContext, useSensor, useSensors} from '@dnd-kit/core';
import {arrayMove, rectSortingStrategy, SortableContext} from '@dnd-kit/sortable';
import MyPointerSensor from './PointerSensorFix'

function DraggableColorList(props) {

    
    const { colors, deleteNewColor } = props

    const [items, setItems] = useState(colors)

    const itemRemove =(deletedItem) => {
        setItems((items) => {
          items = items.filter(item => item.name !== deletedItem)
          return(items)
        })
    }

    const colorList = items.map( oneColor => {
        return <DraggColorBox 
          key={oneColor.name}
          color={oneColor.color}
          colorName={oneColor.name}
          deleteNewColor={deleteNewColor}
          itemRemove={itemRemove}
          id={oneColor.id}
        />
    })

    

    useEffect(()=> {
        if (colors.length > items.length) {

            setItems((items) => {
                items =  [...items, colors[colors.length - 1]] 
                return items     
            })
            
            
        } else if (colors.length === 0 ) {
            setItems( items => items = [])
        }
         

    } , [colors] )  
 
  

    const pointerSensor = useSensor(MyPointerSensor, {
        activationConstraint: {distance: 25}     
    });
   

    const sensors = useSensors(pointerSensor)
   

    const handleDragEnd = ({active, over}) => {
      
        if (active.id !== over.id) {
            
          setItems((items) => {
            const oldIndex = items.findIndex(item => item.id === active.id)
            const newIndex = items.findIndex(item => item.id === over.id)

            
            return arrayMove(items, oldIndex, newIndex)
          })
        }
      }

    return(
        <div style={{height: "100%"}}>
            <DndContext 
            sensors={sensors}
            collisionDetection={closestCenter}
            onDragEnd={handleDragEnd} >
                <SortableContext 
                    items={items.map(oneColor => oneColor.id)}
                    strategy={rectSortingStrategy}
                >
                    {colorList}
                </SortableContext>
            </DndContext>
        </div>
    )  
} 

export default DraggableColorList; 