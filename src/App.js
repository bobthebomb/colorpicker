import './App.css';
import SeedColors from "./SeedColors";
import {Routes, Route} from "react-router-dom"
import NavPalette from './NavPalette';
import PaletteList from "./PaletteList"
import SingleColorPalette from "./SingleColorPalette"
import NewPaletteForm from './NewPaletteForm';
import React, { Component } from 'react';
import { Box } from '@mui/material';
import NotFound from './NotFound';


class App extends Component {
  constructor(props){
    super(props)
    const savedPalettes = JSON.parse(window.localStorage.getItem("palettes"))
    this.state = {
      palettes: savedPalettes || SeedColors
    }
  }


  savePalette = (palette) => { 
    this.setState({
      palettes: [...this.state.palettes, palette]
    } ,this.syncLocalStorage) ;
  }

  deletePalettes = (id) => {
    this.setState({
      palettes: this.state.palettes.filter(palette => palette.id !== id)
    }, this.syncLocalStorage)
  }

  syncLocalStorage = () => {
    window.localStorage.setItem(
      "palettes", 
      JSON.stringify(this.state.palettes)
    )
  }

  // Fade ne fonctionne pas comme wrapper de Routes alors que CSSTransition group oui....
  //React router dom V6 a besoin
  
  render(){
    return (
      
      <Box sx={{height: "100vh"}} >
           
                <Routes>       
               

                  <Route path="/" element={<PaletteList seedColors={this.state.palettes} deletePalettes={this.deletePalettes}/>}  /> 
                  <Route path ="/palette/:paletteId" element={ <NavPalette seedColors={this.state.palettes} />}/>
                  <Route path ="/palette/:paletteId/:colorId" element={ <SingleColorPalette  seedColors={this.state.palettes} />}/>
                  <Route path ="/palette/new" element={<NewPaletteForm  palettes={this.state.palettes} savePalette={this.savePalette} />} />
                  <Route path="*" element={<PaletteList seedColors={this.state.palettes} deletePalettes={this.deletePalettes}/>}  /> 

                </Routes> 
        

   
        
      </Box>
    );

  }

  

  
}

export default App;
