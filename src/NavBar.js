import React, { Component } from 'react';
import Slider from "rc-slider"
import 'rc-slider/assets/index.css';
import "./NavBar.css";
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import Snackbar from '@mui/material/Snackbar';
import IconButton from '@mui/material/IconButton';
import CloseIcon from '@mui/icons-material/Close';
import { FormControl } from '@mui/material';
import { Link } from "react-router-dom";

class NavBar extends Component {
    constructor(props){
        super(props)
        this.state = {
            colorType : "hex",
            open: false
        }
    }

    changeLevel = (newLevel) => {
        this.props.changeLevelParent(newLevel)
    }

    /* Naming bien nase quand ty y pense. J'update le state dans le parent (palette) avec this.props.handleChange et j'update le state dans mon controlled component
    Note : Je n'avais pas besoin de controlled compônent puis que material UI le faisait automatiquement sauf sur le Mount, je suis sur que le soluce doit etre dans la DOC 
    Bref sa marche comme ca, mais j'ai 2 handleChange une dans NavBar et une dans Palette 
    Edit : Finalement c'est named handleFormatChange*/
    
    handleFormatChange = (event) => {
        this.setState({
            colorType : event.target.value,
            open : true
        })
        this.props.handleChange(event.target.value)
    }


    closeSnackbar = () => {
        this.setState({
            open : false
        })
    }

    render(){
        const { level, lvlBarHandle } = this.props
        return(
            <header className="NavBar">
                <div className="logo">
                    <Link to="/"> Zermati Color Pickers </Link>               
                </div>
     
                {lvlBarHandle && <div className="slider-container">
                <span> level: {level}</span>
                    <Slider 
                    
                            defaultValue={level} 
                            min={100} max={900}  
                            step={100} 
                            onAfterChange={this.changeLevel}  
                            trackStyle={{backgroundColor : 'transparent'}}
                            handleStyle={{backgroundColor: 'green', outline: 'none', border:'2px solid green', boxShadow: 'none', width: "13px",  marginTop: '-3px' }}
                            railStyle={{ height: "8px"}}
                    />
                </div> }     
                <div className="Select-container">
                    <FormControl variant="standard">
                        <Select 
                            value={this.state.colorType} 
                            onChange={this.handleFormatChange}
                            labelId="demo-simple-select-standard-label"
                            id="demo-simple-select-standard">
                            
                            <MenuItem value="hex"> Hex - #ffffff</MenuItem>
                            <MenuItem value="rgb"> RGB - rgb(255,255,255) </MenuItem>
                            <MenuItem value="rgba"> RGBA - rgba(255,255,255,1)</MenuItem>
                        </Select>
                    </FormControl>
                    
                </div>
                <Snackbar 
                    anchorOrigin={{vertical: "bottom", horizontal: "left"}} 
                    open={this.state.open} 
                    message={ <span id="message-id"> Format Changed to "{this.state.colorType.toUpperCase()}" </span>  }
                    autoHideDuration={3000}
                    onClose={this.closeSnackbar}
                    action={ [
                                <IconButton 
                                    onClick={this.closeSnackbar}
                                    color="inherit"
                                    key="close"
                                    aria-label="close" > 
                                    <CloseIcon /> 
                                </IconButton>] 
                            }
                 />
                
                
              
            </header>
        )
    }
} export default NavBar;