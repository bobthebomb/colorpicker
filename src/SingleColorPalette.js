import React, {useState} from 'react';
import { Link, useParams} from "react-router-dom"
import {generatePalette} from "./colorHelpers"
import ColorBox from "./ColorBox"
import NavBar from './NavBar';
import PaletteFooter from "./PaletteFooter"
import { styled } from "@mui/system";
import { TransitionGroup,} from 'react-transition-group';
import Fade from '@mui/material/Fade';




function SingleColorPalette(props) {

    /* react roouter dom v6 hook, useParams me donne un obj passé par Route, remplace url params et match des anciennes versions
    Le defaut c'est de pas le voir en props donc c'est plus dur pour debug */ 

    const { colorId, paletteId } = useParams()
    const [ format, setFormat] = useState("hex")
    const fullcolors = generatePalette(findOnePalette())
    const { emoji, paletteName } = fullcolors

    function changeFormat(event){
       setFormat(event)
     }

    function findOnePalette() {
        return props.seedColors.find( color => color.id === paletteId)
    }


    function gatherShade() {
        let shades = [];
        let allColors = fullcolors.colors

        for(let lvlKey in allColors) {
             shades = shades.concat(
                allColors[lvlKey].filter( color => color.id === colorId)
            )
        }
        return shades.slice(1)
    }


   
    const ContainerFullPages = styled("div")({
        height:" 100%",
        display: "flex",
        flexDirection: "column",
            })

    const ContainerPalettes = styled("div")({
        height:"88%", 
        border: "solid 2px black", 
        backgroundColor: "black"
    })

 
    const renderingOneColor = gatherShade().map(color => {
        return <ColorBox 
        colorId={color.id}  
        background={color[format]} 
        name={color.name} 
        key={color.name} 
        moreLink={false}
        />
    })

    return(
        <ContainerFullPages  className=" SingleColorPalette" >

            <NavBar 
                 handleChange={ changeFormat}
                 lvlBarHandle={false}
            />

             <TransitionGroup component={null}>
                <Fade key={`MiniPalette-${colorId}`} timeout={700}>
                    <ContainerPalettes >
                            {renderingOneColor}                    
                            <div className=" ColorBox">                        
                            <Link to={`/${paletteId}`}> <button className="back-button">Go Back</button> </Link> 
                            </div>
                    </ContainerPalettes>

                </Fade>

                <PaletteFooter paletteName={paletteName} emoji={emoji} />

            </TransitionGroup>

        </ContainerFullPages>
       
    )
    
} export default SingleColorPalette
