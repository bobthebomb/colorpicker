import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import MuiAppBar from '@mui/material/AppBar';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import {useState} from "react";
import { Button } from '@mui/material';
import DraggableColorList from './DraggableColorList';
import NewNavBar from './NewNavBar';
import ColorPickerForm from './ColorPickerForm'


let intViewportWidth = window.innerWidth;
console.log(intViewportWidth)

function checkSize() {
  if (window.innerWidth > 395) {
    return 400 
  } else {
    return window.innerWidth
  }
}

const drawerWidth = checkSize();
const barHeight = 64;
const maxSize = 20;



//il faut pense a exporter tous ca dans des folders theme et le faire avec une test suite pour l'integrité des props.

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
     height: `calc(100vh - ${barHeight}px)`,
    padding: 0,
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    // marge a gauche du main quand le drawer et fermé, il est en négatif de la width du drawer pour eviter un décalage et les breakpoint du sx ne fonctionnent pas
    marginLeft: `-${drawerWidth}px`,
    // [theme.breakpoints.up('xl')]:{ marginLeft: "-400px"},
    // [theme.breakpoints.up('lg')]:{ marginLeft: "-400px"},
    // [theme.breakpoints.up('md')]:{ marginLeft: "-400px"},
    // [theme.breakpoints.up('sm')]:{ marginLeft: "-400px"},
    // [theme.breakpoints.up('xs')]:{ marginLeft: "-400px"},

    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }), 
      // marge a gauche du main (les colorbox array) et le drawer open et les breakpoint du sx ne fonctionnent pas
      marginLeft: 0,
    }),
  }),
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));

const DrawerCustomContainer = styled("div")( ({theme}) =>  ({
   
    width: "100%",
    display:"flex",
    flexDirection:"column",
    justifyContent: "center",
    alignItems: "center",
    height: "100%",

}))

const ButtonsParent = styled("div")( ({ theme}) => ({
    width:"90%"
   
}))

const theTwoButtons = {
  width: "50%"
}





function NewPaletteDrawer(props) {

  const [open, setOpen] = useState(false);

  const [CurrentColor, changeColor] = useState("#000000")

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const handleClick = () => {
      props.addNewColor(CurrentColor, props.newColorName)
  }

  
  const handleCompleteChange = (newColor, event ) => {
    console.log( "onCompleteChange" + newColor.hex)

    return changeColor(newColor.hex)
  }

  const handleChange = (evt) => {
    props.addNewName(evt.target.name, evt.target.value)
  }

  const handleClearColors = () => {
    props.clearColors()
  }

  const handleRandomColors = () => {
    props.randomColors()
  }



  return (
    
    <Box sx={{label: "MegaBox", display: 'flex' }}>
      <NewNavBar 
        open={open} 
        AppBar={AppBar} 
        handleDrawerOpen={handleDrawerOpen} 
        savePalette={props.savePalette} 
        handleChange={handleChange}
        palettes={props.palettes}
        newPaletteName={props.newPaletteName}
        colors={props.colors}
        addNewName={props.addNewName}


      />



         
      <Drawer
        sx={{
          //drawer width quand il est fermé pour calculer la marginleft negative du drawer closed dans le main le
          width: drawerWidth,
          flexShrink: 0,
          '& .MuiDrawer-paper': {
            //drawer open size, je le voudrais a 100% sur du 450px et moins mais encore une fois material ui est inconsitant :( )
            width: drawerWidth,
            
            
            boxSizing: 'border-box',
          },
        }}
        variant="persistent"
        anchor="left"
        open={open}
      >

        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
             <ChevronLeftIcon /> 
          </IconButton>
        </DrawerHeader>
        <Divider />
        
        <Divider />
       

      <DrawerCustomContainer>

      <Typography 
        variant="h4"
        gutterBottom
      >
            Design your palette
        </Typography>

        <ButtonsParent>
            <Button 
             sx={theTwoButtons}
              variant="contained" 
              color="secondary" 
              onClick={handleClearColors}
            > 
                Clear Palette 
            </Button>

            <Button 
              variant="contained" 
              color="primary"  
              onClick={handleRandomColors}
              disabled={props.colors.length >= maxSize}
              sx={theTwoButtons}
            > 
              Random Color 
            </Button>

        </ButtonsParent>
        
       
        <ColorPickerForm 
          handleCompleteChange={handleCompleteChange}
          handleChange={handleChange}
          CurrentColor={CurrentColor}
          handleClick={handleClick}
          maxSize={maxSize}
          colors={props.colors}
          newColorName={props.newColorName}
        />

        

</DrawerCustomContainer>

      </Drawer>
      <Main  open={open}>
      <DrawerHeader />
        <DraggableColorList   colors={props.colors} deleteNewColor={props.deleteNewColor} />
      </Main>
    </Box>
  );
} 

export default NewPaletteDrawer;