import React, {memo} from 'react';
import { styled } from "@mui/system";
import DeleteIcon from '@mui/icons-material/Delete';


// prevent the rerendering de minipalette avec le higher class de react ! remplasse le vieux pureComponent & shouldUpdate trick
export default memo( 

  function MiniPalette(props) {

  const { paletteName, id , colors, emoji} = props


  const MiniBoxes = styled('div')({
    width:"20%",
    height:"25%",
    display: "inline-block",
    margin: 0,
    position: "relative",
    marginBottom: "-4px"

  })

  const Root = styled('div')({
    backgroundColor: "white",
    borderRadius: "5px",
    border: "2px solid black",
    padding: "0.5rem",
    position: "relative",
    overflow: "hidden",
    cursor: "pointer",

    "&:hover svg": {
      opacity: 1
    }

  });

  const CssColors = styled('div')({
    backgroundColor : "lightgray",
    height: "150px",
    width:"100%"
  });

  const Title = styled('h5')({
      display: "flex",
      justifyContent: "space-between",
      alignItems: "center",
      margin: "0",
      color: "black",
      paddingTop: "0.5rem",
      fontSize: "1rem",
      position: "relative",
      textDecoration: "none"
  });

  const Emoji = styled('span')({
      marginLeft: "0.5rem",
      fontSize: "1.5rem",
      marginRight: "0.5rem",
      textDecoration: "none"

  });

  const miniColorBoxes = colors.map( color => {
    return (
            <MiniBoxes 
              key={color.name} 
              style={{backgroundColor: color.color}} > 
            </MiniBoxes>
          
            )
  })

  const deleteIconCss = {
    color: "white",
    backgroundColor: "#eb3d30",
    width: "20px",
    height: "20px",
    position: "absolute",
    right: "0px",
    top: "0px",
    padding: "8px",
    zIndex: 50,
    opacity: 0,
    transition: "all 0.3s ease-in-out"
  }

  const handleDeleteClick = (e) => {
    e.preventDefault();
    props.deletePalettes(id)
    
}    


  return(
    
    <Root>
      {console.log(paletteName)}
        <DeleteIcon
        sx={deleteIconCss}
        onClick={handleDeleteClick}
        />
      <CssColors>

            {miniColorBoxes}



      </CssColors> 
        <Title className="title"> 
            {paletteName} 
          <Emoji className="emoji"> 
            {emoji}
          </Emoji> 
        </Title>          
    </Root>
     
  )
}
)



