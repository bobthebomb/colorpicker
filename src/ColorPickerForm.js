import React from 'react';
import { ChromePicker} from "react-color";
import { Button } from '@mui/material';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import { useEffect} from "react";
import chroma from "chroma-js"


function ColorPickerForm(props) {

    const {handleCompleteChange , CurrentColor, handleChange , handleClick,maxSize, colors, newColorName} = props

    const isDarkColor = chroma(CurrentColor).luminance() <= 0.3

    const colorPickerStyle = {
        
        border: "2px solid black"
    }

    const addColorButton = {
        backgroundColor: colors.length >= maxSize ? "lightgrey" : CurrentColor ,
        width: "100%",
        padding: "1rem",
        marginTop: "1rem",
        fontSize: "1.5rem",
        color: isDarkColor ? "primary" : "black"
    }

    const addColorNameInput = {
        marginTop: "1rem",
        width: "100%",
        

    }

    // a rechercher : Pourquoi si j'utilise un styled componend de miu ca defocus le le text validator. 
    // Miu styled component c'est quand même pas ouf, y'a vraiment trop de probleme avec cet HOC api
    //Check Mantine comme replacement ou back to react bootstrap...

    const ContainerDiv = {
        marginTop: "1rem",
        width: "90%"
    }

   

    useEffect(
        ()=>{

            ValidatorForm.addValidationRule('isColorUnique', (value) => {
                return colors.every(
                    ({color}) => color !== CurrentColor
                )
            })


            ValidatorForm.addValidationRule('UniqueName', (value) => {
                return colors.every( 
                    ({name}) => name.toLowerCase() !== value.toLowerCase()
                )
            })   
            
        } 
    )


    return(

        <div style={ContainerDiv}>

            <ValidatorForm 
                onSubmit={handleClick} 
                instantValidate={true}
            >

                <ChromePicker width="100%" style={colorPickerStyle} color={CurrentColor} onChangeComplete={handleCompleteChange} />

            
                <TextValidator 
                    placeholder="Color Name"
                    variant="filled"
                    label="Color Name"
                    onChange={handleChange}
                    name="newColorName"
                    value={newColorName}
                    validators={['required' , 'UniqueName', 'isColorUnique']}
                    errorMessages={['this field is required', 'unique name is required', 'color already used']}
                    sx={addColorNameInput}
                />          

                <Button 
                    variant="contained" 
                    type='submit'                    
                    sx={addColorButton}
                    disabled={colors.length >= maxSize}
                > 

                Add Color 

                </Button>
                
            </ValidatorForm>

        </div>
    )
} 

export default ColorPickerForm ;